# for PyCEGUI
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name:           cegui
Version:        0.7.7
Release:        1%{?dist}
Summary:        Free library providing windowing and widgets for graphics APIs / engines
Group:          System Environment/Libraries
License:        MIT
URL:            http://www.cegui.org.uk
Source0:        http://downloads.sourceforge.net/crayzedsgui/CEGUI-%{version}.tar.gz

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:  DevIL-devel
BuildRequires:  freeimage-devel
BuildRequires:  expat-devel
BuildRequires:  freetype-devel > 2.0.0
BuildRequires:  libxml2-devel
BuildRequires:  libICE-devel
BuildRequires:  libGLU-devel
BuildRequires:  libtool
BuildRequires:  libSM-devel
BuildRequires:  lua-devel >= 0.5.1
BuildRequires:  pcre-devel
BuildRequires:  pkgconfig >= 0.9.0
BuildRequires:  SILLY-devel
BuildRequires:  xerces-c-devel
BuildRequires:  tolua++-devel
BuildRequires:  tinyxml-devel
BuildRequires:  glew-devel
BuildRequires:  ogre-devel >= 1.7.0
BuildRequires:  irrlicht-devel
BuildRequires:  doxygen
BuildRequires:  graphviz
# for python detection
BuildRequires:  python
BuildRequires:  python-devel
BuildRequires:  boost-devel
# filter PyCEGUI modules from Provides
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$ 
%filter_setup
}

%description
Crazy Eddie's GUI System is a free library providing windowing and widgets for
graphics APIs / engines where such functionality is not natively available, or
severely lacking. The library is object orientated, written in C++, and
targeted at games developers who should be spending their time creating great
games, not building GUI sub-systems!


%package devel
Summary:        Development files for cegui
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-DevIL-imagecodec = %{version}-%{release}
Requires:       %{name}-freeimage-imagecodec = %{version}-%{release}
Requires:       %{name}-irrlicht-renderer = %{version}-%{release}
Requires:       %{name}-ogre-renderer = %{version}-%{release}
Requires:       %{name}-libxml-xmlparser = %{version}-%{release}
Requires:       %{name}-tinyxml-xmlparser = %{version}-%{release}
Requires:       %{name}-xerces-xmlparser = %{version}-%{release}
Requires:       libGLU-devel

%description devel
Development files for cegui


%package devel-doc
Summary:        API documentation for cegui
Group:          Documentation
Requires:       cegui-devel = %{version}-%{release}

%description devel-doc
API and Falagard skinning documentation for cegui


%package DevIL-imagecodec
Summary:        Alternative imagecodec library for CEGUI using DevIL
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description DevIL-imagecodec
Alternative imagecodec library for CEGUI using DevIL.


%package freeimage-imagecodec
Summary:        Alternative imagecodec library for CEGUI using freeimage
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description freeimage-imagecodec
Alternative imagecodec library for CEGUI using freeimage.


%package irrlicht-renderer
Summary:        Irrlicht renderer for CEGUI
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description irrlicht-renderer
Irrlicht renderer for CEGUI.


%package ogre-renderer
Summary:        OGRE renderer for CEGUI
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description ogre-renderer
OGRE renderer for CEGUI.


%package libxml-xmlparser
Summary:        Alternative xml parsing library for CEGUI using libxml
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description libxml-xmlparser
Alternative xml parsing library for CEGUI using libxml.


%package tinyxml-xmlparser
Summary:        Alternative xml parsing library for CEGUI using tinyxml
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description tinyxml-xmlparser
Alternative xml parsing library for CEGUI using tinyxml.


%package xerces-xmlparser
Summary:        Alternative xml parsing library for CEGUI using xerces
Group:          System Environment/Libraries
Requires:       cegui = %{version}-%{release}

%description xerces-xmlparser
Alternative xml parsing library for CEGUI using xerces.


%package python
Summary:        Python bindings for CEGUI
Requires:       cegui = %{version}-%{release}
Requires:       boost-python

%description python
Python bindings called PyCEGUI that enable users to use the full API of CEGUI
from python. Contains bindings for the CEGUI library itself and its
OpenGLRenderer (module PyCEGUIOpenGLRenderer).

%prep
%setup -q -n CEGUI-%{version}

# Permission fixes for debuginfo RPM
chmod -x cegui/include/falagard/*.h

# Encoding fixes
iconv -f iso8859-1 doc/README -t utf8 > doc/README.conv
touch -r doc/README doc/README.conv 
mv -f doc/README.conv doc/README


%build
# We patched acinclude.m4 so we need to bootstrap
./bootstrap
%configure --disable-static --disable-corona --disable-samples \
           --disable-directfb-renderer \
           --enable-toluacegui \
           --with-default-xml-parser=ExpatParser \
           --with-default-image-codec=SILLYImageCodec \
           --with-pic
# Don't use rpath!
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags} 'Ogre_LIBS = -L%{_libdir} -lOgreMain -lpthread'
pushd doc/doxygen
doxygen doxyfile
popd


%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot} 
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
# Fedora sadly doesn't ship python-ogre so this module would be useless
find $RPM_BUILD_ROOT -name "PyCEGUIOgreRenderer.so" -exec rm -f {} ';'

%clean
rm -rf %{buildroot}


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc doc/COPYING doc/README
%{_libdir}/libCEGUI*-%{version}.so
%exclude %{_libdir}/libCEGUIDevILImageCodec-%{version}.so
%exclude %{_libdir}/libCEGUIFreeImageImageCodec-%{version}.so
%exclude %{_libdir}/libCEGUIIrrlichtRenderer-%{version}.so
%exclude %{_libdir}/libCEGUIOgreRenderer-%{version}.so
%exclude %{_libdir}/libCEGUILibxmlParser-%{version}.so
%exclude %{_libdir}/libCEGUITinyXMLParser-%{version}.so
%exclude %{_libdir}/libCEGUIXercesParser-%{version}.so

%files devel
%defattr(-,root,root,-)
%{_bindir}/tolua++cegui
%{_libdir}/*.so
%exclude %{_libdir}/libCEGUI*-%{version}.so
%{_libdir}/pkgconfig/CEGUI-OPENGL.pc
%{_libdir}/pkgconfig/CEGUI-OGRE.pc
%{_libdir}/pkgconfig/CEGUI.pc
%{_includedir}/CEGUI
%{_datadir}/CEGUI

%files devel-doc
%defattr(-,root,root,-)
%doc doc/doxygen/html

%files DevIL-imagecodec
%defattr(-,root,root,-)
%{_libdir}/libCEGUIDevILImageCodec-%{version}.so

%files freeimage-imagecodec
%defattr(-,root,root,-)
%{_libdir}/libCEGUIFreeImageImageCodec-%{version}.so

%files irrlicht-renderer
%defattr(-,root,root,-)
%{_libdir}/libCEGUIIrrlichtRenderer-%{version}.so

%files ogre-renderer
%defattr(-,root,root,-)
%{_libdir}/libCEGUIOgreRenderer-%{version}.so

%files libxml-xmlparser
%defattr(-,root,root,-)
%{_libdir}/libCEGUILibxmlParser-%{version}.so

%files tinyxml-xmlparser
%defattr(-,root,root,-)
%{_libdir}/libCEGUITinyXMLParser-%{version}.so

%files xerces-xmlparser
%defattr(-,root,root,-)
%{_libdir}/libCEGUIXercesParser-%{version}.so

%files python
%defattr(-,root,root,-)
%{python_sitearch}/PyCEGUI.so
%{python_sitearch}/PyCEGUIOpenGLRenderer.so


%changelog
* Sun Jul 01 2012 Martin Preisler <mpreisle@redhat.com> 0.7.7-1
- New upstream release

* Mon Jan 23 2012 Martin Preisler <mpreisle@redhat.com 0.7.6-1
- New upstream release
- Added python to build requirements
- Added graphviz to build requirements

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.5-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Tue Nov 20 2011 Bruno Wolff III <bruno@wolff.to> 0.7.5-10
- Rebuild for boost soname bump

* Wed Sep 21 2011 Martin Preisler <mpreisle@redhat.com> 0.7.5-9
- Added the python subpackage (PyCEGUI)

* Tue Jun 21 2011 Bruno Wolff III <bruno@wolff.to> 0.7.5-8
- Rebuild for glew soname bump

* Sun May 15 2011 Bruno Wolff III <bruno@wolff.to> 0.7.5-7
- Rebuild for ogre 1.7.3

* Thu Mar 10 2011 Kalev Lember <kalev@smartlink.ee> - 0.7.5-6
- Rebuilt with xerces-c 3.1

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.5-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Fri Jan  7 2011 Tom Callaway <spot@fedoraproject.org> 0.7.5-4
- rebuild against ogre without poco

* Mon Jan 03 2011 Hans de Goede <hdegoede@redhat.com> 0.7.5-3
- Put the OGRE and Irrlicht renderers in their own subpackages to reduce the
  number of deps of the main cegui package

* Mon Jan 03 2011 Bruno Wolff III <bruno@wolff.to> 0.7.5-2
- Fix typo in ogre dependency

* Tue Dec 21 2010 Tom Callaway <spot@fedoraproject.org> 0.7.5-1
- New upstream release 0.7.5
- Enable support for ogre, irrlicht

* Fri Nov  5 2010 Hans de Goede <hdegoede@redhat.com> 0.7.4-1
- New upstream release 0.7.4
- Also build the freeimage image codec
- Put the non default image codecs (DevIL, freeimage) and xml parsers (libxml,
  tinyxml and xerces) into their own sub-packages to reduce the number of deps
  of the main cegui package

* Mon Jun 21 2010 Hans de Goede <hdegoede@redhat.com> 0.6.2-6
- Fix building with latest tinyxml (#599850)

* Sun Feb 07 2010 Bruno Wolff III <bruno@wolff.to> - 0.6.2-5
- Rebuild for xerces update.

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Mon Feb 23 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.6.2-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Sat Feb 21 2009 Hans de Goede <hdegoede@redhat.com> 0.6.2-2
- Fix building with latest DevIL

* Wed Dec  3 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.6.2-1
- New upstream release 0.6.2

* Fri Jul 11 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.6.1-1
- New upstream release 0.6.1
- Drop upstreamed patches

* Sun May 18 2008 Hans de Goede <j.w.r.degoede@hhs.nl> 0.6.0-1
- New upstream release 0.6.0
- No ABI stability, use full versioned (libtool -release) sonames
- Use system tolua++, change license tag to match
- Use system tinyxml

* Wed Feb 13 2008 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-7
- Added patch for new xerces-c. Courtesy of Hans de Goede
- Converted some documentation to UTF8
- Minor spec cleanups

* Wed Aug 29 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-6
- Yet another release bump, for building against expat2.

* Tue Aug 21 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-5
- Release bump for F8 mass rebuild
- License change due to new guidelines

* Sat Jun 30 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-4
- Release bump

* Sun Jun 17 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-3
- rpath fixes for x86_64

* Sun Jun 10 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-2
- Added patch to fix undefined-non-weak-symbol warnings

* Wed May 30 2007 Ian Chapman <packages[AT]amiga-hardware.com> 0.5.0b-1
- Upgrade to 0.5.0b
- Added patch from Gentoo to compile with lua 5.1
- Updated the patch to use versioned .so for dlopen()
- Dropped several patches as they are no longer needed
- Dropped useless provides. Nothing used them anyway
- Added support for the SILLY image codec
- Added support for xerces-c

* Mon Aug 28 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-11
- Release bump for FC6 mass rebuild

* Sat Aug 05 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-10
- Header fix for g++ v4.1+

* Tue Jul 18 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-9
- Use versioned .so for dlopen()

* Sun Jun 11 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-8
- Updated --rpath fixes again
- Package devel-docs renamed to devel-doc as per 'new' guidelines

* Sat Jun 10 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-7.iss
- Updated --rpath fixes

* Fri Jun 09 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-6.iss
- Added patch courtesy of Hans de Goede fixing TinyXML usage
- Added patch courtest of Hans de Goede fixing 64bit issues
- Updated --rpath fixes
- Trivial correction for pkgconfig BR, should be >= really and not >

* Wed Jun 07 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-5.iss
- Removed IrrlichtRender headers as we don't support it (yet - anyway)
- Removed usage of --rpath during build process
- libtool dropped as a BR (no longer needed due to --rpath fix)
- Moved rebuilding of C++ bindings to %%build section

* Mon Jun 05 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-4.iss
- Added a tentative patch for building with the system tolua++/lua
- Added tolua++-devel as a buildrequire
- Rebuild the C++ bindings using the system tolua++

* Sun May 28 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-3.iss
- Added patch courtesy of Hans de Goede to force compilation against the system
  pcre libs instead of the bundled pcre.
- Added pcre-devel to buildrequires
- Replace xorg-x11-devel with libGLU-devel
- Removed PCRE-LICENSE from doc as it's now compiled against system pcre
- Specified version for pkgconfig buildrequires
- Replaced source URL with primary sf site, rather than a mirror
- Don't use bootstrap
- Added cegui_mk2 provides
- Removed superfluous documentation from devel package

* Sat May 27 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-2.iss
- Use %%{?dist} for most recent changelog entry - avoids incoherent changelog
  versions if %%{?dist} macro is missing or different.
- Added %%{version}-%%{release} to provides field
- Replaced %%{__sed} with sed

* Sun May 21 2006 Ian Chapman <packages[AT]amiga-hardware.com> 0.4.1-1.iss
- Initial Release
